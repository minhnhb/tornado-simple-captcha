=====
Tornado simple captcha
=====

Add captcha to your tornado application

Installation
============
    $ pip install -e git+https://minhnhb@bitbucket.org/minhnhb/tornado-simple-captcha.git#egg=captcha

Usage
=====
    >>> from captcha import SimpleCaptcha
    >>> class CatchaHandler(tornado.web.RequestHandler):
    >>>     def get(self):
    >>>         captcha = SimpleCaptcha(self)
    >>>         captcha.render()
    >>>     def post(self):
    >>>         captcha = SimpleCaptcha(self)
    >>>         captcha_answer = self.get_argument("captcha_answer", "")
    >>>         valid = captcha.validate(captcha_answer)
