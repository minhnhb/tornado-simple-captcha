import os
import random
import StringIO
import Image
import ImageFont
import ImageDraw
import ImageFilter
import settings
from string_utils import random_ascii

_FONT_PATH = os.path.normpath(os.path.join(os.path.dirname(__file__), "captcha.ttf"))

class Captcha:
    _font = ImageFont.truetype(settings.CAPTCHA_FONT_PATH, settings.CAPTCHA_FONT_SIZE)

    def __init__(self, request_handler):
        self.challenge = random_ascii(6).lower()
        self.request_handler = request_handler

    def validate(self, challenge_answer):
        challenge = self.request_handler.get_secure_cookie("_captcha")
        return challenge == challenge_answer.lower().strip()

    def render(self, challenge=None, *args, **kwargs):
        self.challenge = challenge or self.challenge
        text = self.challenge
        """Generate a captcha image"""
        # randomly select the foreground color
        #fgcolor = random.randint(0,0xffff00)
        # make the background color the opposite of fgcolor
        #bgcolor = fgcolor ^ 0xffffff
        bgcolor = kwargs.get("bgcolor", "#FFFCE9")
        fgcolor = kwargs.get("fgcolor", "#666")
        # determine dimensions of the text
        dim = Captcha._font.getsize(text)
        # create a new image slightly larger that the text
        im = Image.new('RGB', (dim[0]+5,dim[1]+5), bgcolor)
        d = ImageDraw.Draw(im)
        x, y = im.size
        r = random.randint
        # draw 3 random lines on the background
        for num in range(2):
            d.line((r(0,x/2),r(0,y/2),r(0,x),r(0,y)),width=1,fill=0x999)
        # add the text to the image
        d.text((3,3), text, font=Captcha._font, fill=fgcolor)
        im = im.filter(ImageFilter.EDGE_ENHANCE_MORE)
        buf = StringIO.StringIO()
        im.save(buf, format='JPEG')
        jpeg = buf.getvalue()
        buf.close()

        self.request_handler.set_secure_cookie("_captcha", self.challenge)
        self.request_handler.set_header("Content-Type", "image/jpeg")
        self.request_handler.write(jpeg)
