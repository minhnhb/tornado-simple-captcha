import os

CAPTCHA_FONT_PATH = os.path.normpath(os.path.join(os.path.dirname(__file__), 'fonts', 'captcha.ttf'))
CAPTCHA_FONT_SIZE = 25
