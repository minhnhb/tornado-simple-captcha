import os
from setuptools import setup, find_packages

main_ns = {}
ver_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'captcha/version.py')
ver_file = open(ver_path)
exec(ver_file.read(), main_ns)
ver_file.close()

install_requires = [
    'pil',
]

setup(
    name="tornado-simple-captcha",
    version=main_ns["__version__"],
    description='A very simple tornado captcha',
    packages=find_packages(),
    install_requires=install_requires,
)
